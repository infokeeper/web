.PHONY: all create start sysd-start stop restart attach

PROJECTNAME := web
RUNARGS := -p 443:443 -p 80:80 -v `pwd`:/web -v /certs:/certs

default: create

create:
	docker build -t $(PROJECTNAME) .

start:
	docker run -it --name $(PROJECTNAME) $(RUNARGS) -d $(PROJECTNAME)

sysd-start:
	docker run -i --name $(PROJECTNAME) $(RUNARGS) $(PROJECTNAME)

sysd-restart:
	systemctl daemon-reload; \
	systemctl restart $(PROJECTNAME).service

stop:
	docker kill $(PROJECTNAME); \
	docker rm $(PROJECTNAME)

restart: stop start

attach:
	docker exec -i -t $(PROJECTNAME) bash

remote-reload:
	ssh root@antoinepourchet.com 'systemctl daemon-reload; systemctl restart $(PROJECTNAME).service'
