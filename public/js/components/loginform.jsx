import React from 'react';
import { AuthUtils } from '../lib/authutils.jsx';

export class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {email: ''};
    }

    render() {
        return (
                <form id="loginform" style={{width: 200 + 'px'}}>
                    <fieldset className="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" className="form-control" id="exampleInputEmail1" placeholder="Enter email" onChange={this.emailChange.bind(this)}/>
                        <small className="text-muted">We'll never share your email with anyone else.</small>
                    </fieldset>
                    <button type="button" className="btn btn-primary" onClick={this.onSubmitEmail.bind(this)}>Submit</button>
                </form> 
        );
    }

    emailChange(evt) {
        this.setState({email: evt.target.value});
    }

    onSubmitEmail() {
        AuthUtils.TwoFactorStart(this.state.email, function(success) {
            if (success) {
                console.log("Two Factor sent");
            } else {
                console.log("Two Factor not sent");
            }
        });
        console.log("LoginForm onSubmitEmail: " + JSON.stringify(this.state));
    }
    
    onAuthAttempt(success) {
        console.log("LoginForm onAuthAttempt: " + success);
        console.log(JSON.stringify(this.state));
    }

}
