import React from 'react';
import { AuthUtils } from '../lib/authutils.jsx';
import { LoginForm } from './loginform.jsx';

class Body extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (<div><h1>Worlds</h1><LoginForm/></div>); 
    }
}
 
React.render(<Body/>, document.body);
