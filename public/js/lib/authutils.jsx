export class AuthUtils {
    static TwoFactorStart(email, callback) {
        $.post("http://localhost:9998/twofactor", {email: email}, function(result) {
            var data = JSON.parse(result);
            callback(data.success == 1);
        });
    }

    static TwoFactorCheck(email, keys) {
        $.get("localhost:9998/authenticate", {email: email, keys: keys}, function(result) {
            var data = JSON.parse(result);
            callback(data.success == 1); 
        });
    }

    // static Auth(email, callback) {
    //     var ws = new WebSocket("ws://localhost:9998/auth");
    //     ws.onopen = function() {
    //         console.log("AuthUtils Auth: Connection Opened.");
    //         ws.send(email);
    //     };
    //     
    //     ws.onmessage = function (evt) {
    //         console.log("AuthUtils Auth: Message is received: " + evt.data);
    //         var data = JSON.parse(evt.data);
    //         callback(data.success);
    //     };
    //      
    //     ws.onclose = function() { 
    //         console.log("AuthUtils Auth: onClose"); 
    //         callback(false);
    //     }; 

    //     ws.onerror = function(err) { 
    //         console.log("AuthUtils Auth: onError - " + JSON.stringify(err)); 
    //         callback(false);
    //     };

    //     return function(keys) {
    //         console.log("AuthUtils Auth: Sending Keys");
    //         ws.send(keys);
    //     };
    // }
}
